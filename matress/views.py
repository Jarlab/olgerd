from django.shortcuts import render_to_response
from .models import Matress


def home(request):
    matress_list = Matress.objects.all()
    return render_to_response('home.html', {'matress_list': matress_list})
