from django.contrib import admin
from .models import Manufacturer, Matress, SpringBlock, Hardness, Size


class ManufacturerAdmin(admin.ModelAdmin):
    list_display = ('title', 'country')

admin.site.register(Matress)
admin.site.register(Manufacturer)
admin.site.register(SpringBlock)
admin.site.register(Hardness)
admin.site.register(Size)