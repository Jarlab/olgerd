from django.db import models


class Manufacturer(models.Model):
    title = models.CharField(max_length=80)
    country = models.CharField(max_length=30)

    def __str__(self):
        return self.title

class Size(models.Model):
    width = models.IntegerField()
    length = models.IntegerField()
    height = models.IntegerField(blank=True,default=0)

    def __str__(self):
        return str(self.width)+"x"+str(self.length)

class SpringBlock(models.Model):
    title = models.CharField(max_length=40)

    def __str__(self):
        return self.title


class Hardness(models.Model):
    title = models.CharField(max_length=20)
    value = models.IntegerField()

    def __str__(self):
        return self.title


class Matress(models.Model):
    model = models.CharField(max_length=80)
    manufacturer = models.ForeignKey(Manufacturer)
    size = models.ForeignKey(Size)
    spring_block = models.ForeignKey(SpringBlock)
    max_load = models.IntegerField()
    hardness = models.ForeignKey(Hardness)
    price = models.IntegerField()
    photo = models.ImageField(blank=True)
    description = models.TextField(default="")

    def __str__(self):
        return self.model
